#!/usr/bin/env bash

apt-get update

apt-get install -y apt-utils
apt-get install -y sudo

wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
apt install -y ./google-chrome-stable_current_amd64.deb

apt-get install -y maven curl unzip
curl -sL https://deb.nodesource.com/setup_12.x | bash -
apt-get install -y nodejs


mkdir /etc/minerva/
mkdir /etc/minerva/test

#configure database
echo "database.driver=org.hsqldb.jdbcDriver" >>/etc/minerva/db.properties
echo "database.dialect=org.hibernate.dialect.HSQLDialect" >>/etc/minerva/db.properties
echo "database.uri=jdbc:hsqldb:mem:dapi" >>/etc/minerva/db.properties
echo "database.username=sa" >>/etc/minerva/db.properties
echo "database.password=" >>/etc/minerva/db.properties
echo "database.connectionQuery=SELECT 1 FROM INFORMATION_SCHEMA.SYSTEM_USERS" >>/etc/minerva/db.properties
echo "database.pathToDataScripts=/etc/minerva/test" >>/etc/minerva/db.properties

#download test data (database and images)
wget https://git-r3lab.uni.lu/ewa.smula/testing/raw/master/testData/generated_test_data.zip
#wget http://siret.ms.mff.cuni.cz/hoksza/temp/generated_test_data.zip
unzip generated_test_data.zip
mv *.sql /etc/minerva/test
mv map_images /usr/local/tomcat/webapps/

#fetch latest minerva (from master branch)
cd /
git clone https://git-r3lab.uni.lu/minerva/core.git
cd core
#build minerva and deploy it
mvn -Dmaven.test.skip=true clean install -pl web -am
mv web/target/*.war /usr/local/tomcat/webapps/minerva.war

wget https://chromedriver.storage.googleapis.com/2.41/chromedriver_linux64.zip
unzip chromedriver_linux64.zip

mv chromedriver /usr/bin/chromedriver
chown root:root /usr/bin/chromedriver
chmod +x /usr/bin/chromedriver

apt-get install -y vim
apt-get install -y mc