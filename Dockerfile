FROM tomcat:9-jdk11

ADD docker-init.sh /

RUN /docker-init.sh
