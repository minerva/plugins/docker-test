# Docker file used in plugins testing


### General description of the plugins testing procedure

When testing MINERVA plugins, the testing happens on a [dedicated map](https://git-r3lab.uni.lu/ewa.smula/testing) 
(`singleTestMap`) and  a special version of MINERVA running an in-memory database containing this dedicated map . 
The tests are carried out within the continuous integration (CI) pipeline, which sets up a Docker image which then 
runs the in-memory version of MINERVA with this dedicated map. 

### Tests structure

The testing itself consists of JavaScript-based testing of the plugin's functionality. The technologies on which
the testing is built are:

- [Mocha JavaScript testing framework](https://mochajs.org/)
- [Chai assertion library](https://www.chaijs.com/)
- [Selenium](https://www.selenium.dev/)
- [ChromeDriver](https://chromedriver.chromium.org/)
- [Selenium WebDriver](https://www.selenium.dev/projects/)

The tests are then written in JavaScript and executed by Mocha (running within the [Node.js](https://nodejs.org/en/)).
The procedure is as follows:

- Mocha starts the js file with the tests (by default the ``tests/test.js`` file in given plugin directory)
- The Selenium WebDriver JS library is initialized
- The driver intializes ChromeDriver (i.e. starts browser = Chromium) in headless mode (although for the purpose
of tests development, it is possible and often useful to start Chromium in the GUI mode so that one can see
what is going on) and passes necessary arguments and user preferences.
- Credentials for MINERVA are obtained using MINERVA API so that they are available for any subsequent calls
- The plugins hash code is retrieved from MINERVA API. The plugin needs to be already loaded in the
in-memory instance of MINERVA containing the map against which the tests are to be performed. The loading 
of the plugin happens in the CI before the tests are executed; 
or by hand if one is developing the tests and runs them against
local instance of MINERVA.
- The driver loads the map (URI consists of the map address and hash code of the plugin to be tested)
- The individual tests are exectued by Mocha. The tests are basically JS functions which use the Selenium WebDriver
to control the ChromeDriver. 
- The WebDriver closes the browser

The Selenium WebDriver enables to control the driver and also simulate user events. This happens by wrapping
individual elements of the DOM tree by [WebElement](https://www.selenium.dev/selenium/docs/api/java/org/openqa/selenium/WebElement.html).
The elements can be selected by ID, CSS or XPATH and operations such as SendKey can be performed over
a selected element. However, in many cases, the provided functionality is not sufficient
and in such cases the tests use the ability of the driver to execute an arbitrary JS code (so one can use, 
for example, jQuery).

Considering the test are in the ``tests`` directory of the plugin directory, one can run the tests using:

```bash
npx mocha tests
```

or when the ``test`` target is defined in the ``package.json``, one can run:

```bash
npm run test
```

### Tests development

To be able to develop the tests efficiently, it makes sense to have a Docker instance
against which the test can be run (instead of running CI/CD in the development phase). 
That is the purpose of the scripts in this repo; to create a Docker image
with set up environment which can then be started and run tests against it without the need to recreate
the full image, which is quite time-consuming. 

To build the image, run:

```bash
docker build -t docker-test .
```

This will create a docker image called `docker-test`. which installs Google Chrome, Maven, Node.js, 
configures MINERVA database, downloads and uploads the test map to tomcat, clones the MINERVA in-memory
repository and installs Chrome Driver.


With the `docker-test` in place, one can test the plugins 
(let's say the [starter-kit](https://git-r3lab.uni.lu/minerva/plugins/starter-kit))
offline using the following procedure:

 - Create `tests` folder in the plugin directory
 - Create the test files in the `tests` directory
 - Create Dockerfile in the root of the plugin directory (see the `Dockerfile-tests` in the `starter-kit`) that
    - is built from the `docker-test` image
    - attaches the plugin directory
    - starts a script that
        - installs and builds the plugin
        - copies the executable to the Tomcat directory
        - starts the Tomcat server, i.e. MINERVA instance
        - logs in to MINERVA 
        - injects the plugin to MINERVA
        - starts the tests

To build the image, run:

```bash
docker build -t starter-kit -f ./Dockerfile-tests .
```

This will build the image and run the test. If you need to make adjustments to the test and run them again, 
you can start the image, modify the files and run the tests again:

- ```docker run -p 8080:8080 -it starter-kit /bin/bash```
- make adjustments
- start the server and inject the plugin
```bash
PLUGIN_FILE=plugin.js
PLUGIN_MD5=($(md5sum dist/plugin.js))

/usr/local/tomcat/bin/startup.sh
sleep 10

curl -X POST -d "login=admin&password=admin" --write-out %{http_code} --silent --output /dev/null -c cookie.txt http://localhost:8080/minerva/api/doLogin
curl "http://localhost:8080/minerva/api/plugins/" --cookie cookie.txt --data "hash=$PLUGIN_MD5&url=http%3A%2F%2Flocalhost%3A8080%2Ftest%2F$PLUGIN_FILE&name=test&version=0.0.1&isPublic=false"

npm run tests
```

### Tests deployment to CI

In order to make a plugin test available for the CI, the ``.gitlab-ci.yml`` needs to include a test stage 
that is basically concatenation of the ``docker-init.sh`` file found in this repo and
 ``test/docker-init.sh`` file of the plugin:

````bash
test:
  image: tomcat:9-jdk11

  stage: test

  script:
  - apt-get update
  - wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
  - DEBIAN_FRONTEND=noninteractive apt install -y ./google-chrome-stable_current_amd64.deb

  - DEBIAN_FRONTEND=noninteractive apt-get install -y maven curl unzip
  - curl -sL https://deb.nodesource.com/setup_12.x | bash -
  - DEBIAN_FRONTEND=noninteractive apt-get install -y nodejs

  - wget https://chromedriver.storage.googleapis.com/2.41/chromedriver_linux64.zip
  - unzip chromedriver_linux64.zip
  - mv chromedriver /usr/bin/chromedriver
  - chown root:root /usr/bin/chromedriver
  - chmod +x /usr/bin/chromedriver

  - npm install
  - npm run build
  - mkdir /usr/local/tomcat/webapps/test
  - cp dist/plugin.js /usr/local/tomcat/webapps/test/
  - PLUGIN_FILE=plugin.js
  - PLUGIN_MD5=($(md5sum dist/plugin.js))
  - echo $PLUGIN_FILE $PLUGIN_MD5

  - PLUGIN_DIR=`pwd`

  - mkdir /etc/minerva/
  - mkdir /etc/minerva/test

  #configure database
  - echo "database.driver=org.hsqldb.jdbcDriver" >>/etc/minerva/db.properties
  - echo "database.dialect=org.hibernate.dialect.HSQLDialect" >>/etc/minerva/db.properties
  - echo "database.uri=jdbc:hsqldb:mem:dapi" >>/etc/minerva/db.properties
  - echo "database.username=sa" >>/etc/minerva/db.properties
  - echo "database.password=" >>/etc/minerva/db.properties
  - echo "database.connectionQuery=SELECT 1 FROM INFORMATION_SCHEMA.SYSTEM_USERS" >>/etc/minerva/db.properties
  - echo "database.pathToDataScripts=/etc/minerva/test" >>/etc/minerva/db.properties

  #download test data (database and images)
  - wget https://git-r3lab.uni.lu/ewa.smula/testing/raw/master/testData/generated_test_data.zip
  - unzip generated_test_data.zip
  - mv *.sql /etc/minerva/test
  - mv map_images /usr/local/tomcat/webapps/

  #fetch latest minerva (from master branch)
  - cd /
  - git clone -b 1016-refactor-test-framework --single-branch https://git-r3lab.uni.lu/minerva/core.git
  - cd core
  #build minerva and deploy it
  - mvn -Dmaven.test.skip=true clean install -pl web -am
  - mv web/target/*.war /usr/local/tomcat/webapps/minerva.war
  - /usr/local/tomcat/bin/startup.sh
  - sleep 15

  - curl -X POST -d "login=admin&password=admin" --write-out %{http_code} --silent --output /dev/null -c cookie.txt http://localhost:8080/minerva/api/doLogin
  - curl "http://localhost:8080/minerva/api/plugins/" --cookie cookie.txt --data "hash=$PLUGIN_MD5&url=http%3A%2F%2Flocalhost%3A8080%2Ftest%2F$PLUGIN_FILE&name=test&version=0.0.1&isPublic=false"

  #run test
  - cd ${PLUGIN_DIR}
  - npm run test
````

